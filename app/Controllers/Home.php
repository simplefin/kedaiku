<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		// $all_pekan = [
		// 	[
		// 		'nama' => 'Seremban',
		// 		'gambar' => 'https://images.unsplash.com/photo-1586106568539-892de4c2efb4?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8bmVnZXJpfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	], [
		// 		'nama' => 'Gemas',
		// 		'gambar' => 'https://images.unsplash.com/photo-1585665936960-e7c7906262ef?ixid=MXwxMjA3fDB8MHxzZWFyY2h8N3x8bmVnZXJpfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	],  [
		// 		'nama' => 'Kuala Pilah',
		// 		'gambar' => 'https://images.unsplash.com/photo-1563699926131-d0054830f1af?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjR8fG5lZ2VyaXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	],  [
		// 		'nama' => 'Juaseh',
		// 		'gambar' => 'https://images.unsplash.com/photo-1563699925863-42bb73f07583?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzJ8fG5lZ2VyaXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	],  [
		// 		'nama' => 'Rembau',
		// 		'gambar' => 'https://images.unsplash.com/photo-1605775888025-8c80cea3b8a8?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NDd8fG5lZ2VyaXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	],  [
		// 		'nama' => 'Port Dickson',
		// 		'gambar' => 'https://images.unsplash.com/photo-1615870794396-7fde1639f0d2?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzV8fG5lZ2VyaXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
		// 		'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit deleniti ab incidunt ratione excepturi fuga labore, minima tenetur molestias possimus sit reprehenderit odio quia pariatur, veniam ex? Incidunt, natus autem.',
		// 	]
		// ];

		$db = db_connect();

		$result = $db->query('SELECT * FROM gambar ORDER BY nama asc' );
		$all_pekan = $result->getResult();

		//dd( $all_pekan );
	
		return view('homepage', [ 'all_pekan' => $all_pekan ]);
	}

	function hello() {
		echo "<h1>Hello...</h1>";
	}
}
